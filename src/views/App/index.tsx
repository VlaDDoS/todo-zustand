import { useToDoStore } from '../../data/stores/useToDoStore';
import { CreateTask } from '../components/CreateTask';
import { TaskList } from '../components/TaskList';
import styles from './index.module.scss';

export const App = () => {
  const [tasks, createTask, updateTask, completedTask, removeTask] =
    useToDoStore((state) => [
      state.tasks,
      state.createTask,
      state.updateTask,
      state.completeTask,
      state.removeTask,
    ]);

  return (
    <article className={styles.article}>
      <h1 className={styles.article__title}>To do App</h1>
      
      <section className={styles.article__section}>
        <CreateTask onAdd={createTask} />
      </section>

      <hr className={styles.article__hr} />

      <section className={styles.article__section}>
        {!tasks.length ? (
          <p className={styles.article__text}>There is no one task.</p>
        ) : (
          tasks.map((task) => (
            <TaskList
              key={task.id}
              {...task}
              onDone={completedTask}
              onEdited={updateTask}
              onRemove={removeTask}
            />
          ))
        )}
      </section>
    </article>
  );
};
