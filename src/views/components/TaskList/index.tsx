import { FC, KeyboardEvent, useEffect, useRef, useState } from 'react';

import styles from './index.module.scss';

interface TaskListProps {
  id: string;
  title: string;
  completed: boolean;
  onDone: (id: string, completed: boolean) => void;
  onEdited: (id: string, title: string) => void;
  onRemove: (id: string) => void;
}

export const TaskList: FC<TaskListProps> = ({
  id,
  title,
  completed,
  onDone,
  onEdited,
  onRemove,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [titleTask, setTitleTask] = useState(title);

  const editInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (isEditing) {
      editInputRef.current?.focus();
    }
  }, [isEditing]);

  const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      handleSave();
    }
  };

  const handleSave = () => {
    onEdited(id, titleTask);
    setIsEditing(false);
  };

  const handleDelete = () => {
    if (confirm('Are you sure?')) {
      onRemove(id);
    }
  };

  return (
    <div className={styles.list}>
      <label className={styles.list__label}>
        <input
          type="checkbox"
          className={styles.list__checkbox}
          disabled={isEditing}
          checked={completed}
          onChange={(e) => onDone(id, e.target.checked)}
        />

        {isEditing ? (
          <input
            type="text"
            className={styles.list__input}
            value={titleTask}
            ref={editInputRef}
            onChange={(e) => setTitleTask(e.target.value)}
            onKeyDown={handleKeyDown}
          />
        ) : (
          <h3 className={styles.list__title}>
            {completed ? <s>{title}</s> : title}
          </h3>
        )}
      </label>

      {isEditing ? (
        <button
          aria-label="Save"
          className={styles.list__save}
          onClick={handleSave}
        />
      ) : (
        <button
          aria-label="Edit"
          className={styles.list__edit}
          onClick={() => setIsEditing((prev) => !prev)}
        />
      )}

      <button
        aria-label="Delete"
        className={styles.list__delete}
        onClick={handleDelete}
      />
    </div>
  );
};
