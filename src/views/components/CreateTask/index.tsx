import { FC, KeyboardEvent, useState } from 'react';
import styles from './index.module.scss';

interface CreateTaskProps {
  onAdd: (title: string) => void;
}

export const CreateTask: FC<CreateTaskProps> = ({ onAdd }) => {
  const [taskTitle, setTaskTitle] = useState('');

  const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      handleClick();
    }
  };

  const handleClick = () => {
    onAdd(taskTitle);
    setTaskTitle('');
  };

  return (
    <div className={styles.create}>
      <input
        type="text"
        className={styles.create__input}
        placeholder="Type here..."
        value={taskTitle}
        onChange={(e) => setTaskTitle(e.target.value)}
        onKeyDown={handleKeyDown}
      />

      <button
        className={styles.create__btn}
        aria-label="Add"
        onClick={handleClick}
      />
    </div>
  );
};
