import create from 'zustand';
import { devtools } from 'zustand/middleware';
import { generateId } from '../helpers';

interface Task {
  id: string;
  title: string;
  completed: boolean;
  createdAt: number;
}

interface ToDoStore {
  tasks: Task[];
  createTask: (title: string) => void;
  updateTask: (id: string, title: string) => void;
  completeTask: (id: string, completed: boolean) => void;
  removeTask: (id: string) => void;
}

export const useToDoStore = create<ToDoStore>()(
  devtools((set, get) => ({
    tasks: [],

    createTask: (title) => {
      const { tasks } = get();

      const newTask: Task = {
        id: generateId(),
        title,
        completed: false,
        createdAt: Date.now(),
      };

      set({
        tasks: [newTask, ...tasks],
      });
    },

    updateTask: (id, title) => {
      const { tasks } = get();

      const updatedTitle = tasks.map((task) => ({
        ...task,
        title: task.id === id ? title : task.title,
      }));

      set({
        tasks: updatedTitle,
      });
    },

    completeTask: (id, completed) => {
      const { tasks } = get();

      const completedTask = tasks.map((task) => ({
        ...task,
        completed: task.id === id ? completed : task.completed,
      }));

      set({
        tasks: completedTask,
      });
    },

    removeTask: (id) => {
      const { tasks } = get();

      set({
        tasks: tasks.filter((task) => task.id !== id),
      });
    },
  }))
);
